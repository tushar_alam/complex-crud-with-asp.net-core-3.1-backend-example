﻿using Microsoft.EntityFrameworkCore;
using s3Innovate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace s3Innovate.Dbcontext
{
    public class S3InnovateDbContext : DbContext
    {
        public S3InnovateDbContext(DbContextOptions<S3InnovateDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public DbSet<Language> Languages { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Trade> Trades { get; set; }

        public DbSet<TestModel> TestModels { get; set; }
        public DbSet<TestModelDetail> TestModelDetails { get; set; }

    }
}
