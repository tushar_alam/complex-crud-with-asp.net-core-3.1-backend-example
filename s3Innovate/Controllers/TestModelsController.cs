﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using s3Innovate.Dbcontext;
using s3Innovate.Models;
using s3Innovate.Models.ViewModels;

namespace s3Innovate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestModelsController : ControllerBase
    {
        private readonly S3InnovateDbContext _context;

        public TestModelsController(S3InnovateDbContext context)
        {
            _context = context;
        }

        // GET: api/TestModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TestModel>>> GetTestModels()
        {
            return await _context.TestModels.Include(x => x.TestModelDetail).ToListAsync();
        }


        [HttpPost("Search")]
        public async Task<IActionResult> Search([FromBody]DataTable<SearchVm, TestModel> searchVm)
        {
            if (searchVm == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            var dataList = _context.TestModels
                .Include(x => x.Trade)
                .Include(x => x.Level).AsQueryable();

            var vm = searchVm.SearchModel;

            if (vm.TradeId > 0 && vm.TradeId != null)
            {
                dataList = dataList.Where(m => m.TradeId == vm.TradeId);
            }
            if (vm.LevelId > 0 && vm.LevelId != null)
            {
                dataList = dataList.Where(m => m.LevelId == vm.LevelId);
            }


            var totalCount = await dataList.CountAsync();
            var filteredDataList = await dataList.Skip(searchVm.StartPoint).Take(searchVm.EndPoint).ToListAsync();

            if (filteredDataList != null && filteredDataList.Any())
            {
                foreach (var data in filteredDataList)
                {
                    await _context.Entry(data)
                                .Collection(c => c.TestModelDetail)
                                .Query()
                                .Include(x => x.Language)
                                .LoadAsync();

                    foreach (var detail in data.TestModelDetail)
                    {
                        detail.TestModel = null;
                    }
                }
            }


            var dataTable = new DataTable<SearchVm, TestModel>()
            {
                DataList = filteredDataList,
                Count = totalCount,
            };

            return Ok(dataTable);
        }





        // GET: api/TestModels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TestModel>> GetTestModel(int id)
        {
            var testModel = await _context.TestModels.FirstOrDefaultAsync(x => x.Id == id);

            await _context.Entry(testModel)
                   .Collection(c => c.TestModelDetail)
                   .Query()
                   .Include(x => x.Language)
                   .LoadAsync();

            foreach (var detail in testModel.TestModelDetail)
            {
                detail.TestModel = null;
            }


            if (testModel == null)
            {
                return NotFound();
            }

            return testModel;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTestModel(int id, TestModel testModel)
        {
            if (id != testModel.Id)
            {
                return BadRequest();
            }

            var oldTestModel = _context.TestModels.FirstOrDefault(x => x.Id == id);

            oldTestModel.SyllabusName = testModel.SyllabusName;
            oldTestModel.DevelopmentOfficer = testModel.DevelopmentOfficer;
            oldTestModel.Manager = testModel.Manager;
            oldTestModel.ActiveDate = testModel.ActiveDate.Date;


            if (testModel.SyllabusFile != null && testModel.SyllabusFile != "")
            {
                oldTestModel.SyllabusFile = testModel.SyllabusFile;
            }
            if (testModel.TestPlaneFile != null && testModel.TestPlaneFile != "")
            {
                oldTestModel.TestPlaneFile = testModel.TestPlaneFile;
            }


            oldTestModel.TradeId = testModel.TradeId;
            oldTestModel.LevelId = testModel.LevelId;

            if (oldTestModel.TestModelDetail != null && oldTestModel.TestModelDetail.Any())
            {
                _context.TestModelDetails.RemoveRange(oldTestModel.TestModelDetail);
                _context.SaveChanges();
            }

            oldTestModel.TestModelDetail = testModel.TestModelDetail;

            //_context.Entry(testModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [HttpPost]
        public async Task<ActionResult<TestModel>> PostTestModel(TestModel testModel)
        {
            if (testModel != null)
            {
                testModel.ActiveDate = testModel.ActiveDate.Date;
                _context.TestModels.Add(testModel);
                await _context.SaveChangesAsync();

                return NoContent();
            }
            return BadRequest();
        }



        [HttpPost("upload"), DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Files");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }


        // DELETE: api/TestModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TestModel>> DeleteTestModel(int id)
        {
            var testModel = await _context.TestModels.FindAsync(id);
            if (testModel == null)
            {
                return NotFound();
            }

            _context.TestModels.Remove(testModel);
            await _context.SaveChangesAsync();

            return testModel;
        }


        private bool FileValidation(IFormFile file)
        {
            IList<string> allowedFileExtensions = new List<string> { ".pdf", ".txt", ".docx", ".xls", ".xlsx" };
            var ext = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            var extension = ext.ToLower();

            if (allowedFileExtensions.Contains(extension))
            {
                return true;
            }
            return false;
        }

        private byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }

        //private async Task<string> UploadFile(IFormFile ufile)
        //{
        //    if (ufile != null && ufile.Length > 0)
        //    {
        //        var fileName = Path.GetFileName(ufile.FileName);
        //        var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\files", fileName);
        //        using (var fileStream = new FileStream(filePath, FileMode.Create))
        //        {
        //            await ufile.CopyToAsync(fileStream);
        //        }
        //        return filePath;
        //    }
        //    return "";
        //}

        private bool TestModelExists(int id)
        {
            return _context.TestModels.Any(e => e.Id == id);
        }
    }
}
