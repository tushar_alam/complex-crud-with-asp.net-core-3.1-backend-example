﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace s3Innovate.Models
{
    public class TestModelDetail
    {
        public int Id { get; set; }

        [Required]
        public int TestModelId { get; set; }
        public TestModel TestModel { get; set; }

        [Required]
        public int LanguageId { get; set; }
        public Language Language { get; set; }
    }
}
