﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace s3Innovate.Models.ViewModels
{
    public class TestModelVm
    {
        public int Id { get; set; }
        [Required]
        public string SyllabusName { get; set; }
        [Required]
        public string DevelopmentOfficer { get; set; }
        [Required]
        public string Manager { get; set; }
        [Required]
        public string ActiveDate { get; set; }

        public IFormFile SyllabusFile { get; set; }
        public IFormFile TestPlaneFile { get; set; }

        [Required]
        public int TradeId { get; set; }
        [Required]
        public int LevelId { get; set; }

        public Trade Trade { get; set; }
        public Level Level { get; set; }

        public List<TestModelDetail> TestModelDetail { get; set; }
    }

    public class File
    {
        public string name { get; set; }
        public string type { get; set; }
        public string base64 { get; set; }
    }
}
