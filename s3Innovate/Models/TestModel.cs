﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace s3Innovate.Models
{
    public class TestModel
    {
        public int Id { get; set; }
        [Required]
        public string SyllabusName { get; set; }
        [Required]
        public string DevelopmentOfficer { get; set; }
        [Required]
        public string Manager { get; set; }
        [Required]
        public DateTime ActiveDate { get; set; }

        public string SyllabusFile { get; set; }
        public string TestPlaneFile { get; set; }

        [Required]
        public int TradeId { get; set; }
        [Required]
        public int LevelId { get; set; }

        public Trade Trade { get; set; }
        public Level Level { get; set; }

        public List<TestModelDetail> TestModelDetail { get; set; }
    }
}
